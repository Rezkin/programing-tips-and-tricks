"""Standalone scripts that can be used as is or modified depending on the needs of the project.

This module will contain pretty much any odd script, and has no particular theme.  Any larger
classes or functions will have their own module, so this is mostly small, manageable code.
"""
