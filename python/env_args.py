"""This module contains all classes and functions necessary to process environment variables as part
of `argparse`.

Originally based off the solutions provided here:
https://stackoverflow.com/questions/10551117/setting-options-from-environment-variables-when-using-argparse
"""


# Python built-in modules.
import argparse
import os
import sys

# Import to allow for more specific type hinting.
from typing import Any

# Constants used throughout the file.
URL_ENV_NAME = "URL"
IGNORE_ENV_NAME = "IGNORE"
SUCCESS = 0


class EnvironmentVariable(argparse.Action):
    """Provides a way to allow collection of `argparse` variables from the system's environment.

    You simply need to provide this class as an `action`, and specify the name of the environment
    variable to check via `env_name`.  If found, the environment variable will only be used if
    there is no value specified on the command-line.  As such, you can specify required arguments
    that aren't provided to the command line so long as the environment variable exists in the
    system.  If it doesn't, you will get the standard `argparse` error and help text output.

    If an argument is not required, you can specify a default which will be used if no environment
    variable is found.  The environment variable will simply overwrite the default if it is found.
    """

    def __init__(self, env_name: str, required: bool = True, default: Any = None, **kwargs) -> None:
        """Setup and initialize the class.

        This will perform the actual search for an environment variable before calling the parent's
        constructor.

        Args:
            env_name (str): The name of environment variable to pull a value from.  This must be
            the exact value, and case is important.

        KArgs:
            required (bool, optional): Whether or not this variable must exist.  If it the value is
            required and it doesn't exist, and error will be thrown by `argparse`.
            Defaults to True.

            default (Any, optional): A default value to be provided if the value is not required and
            it was not found.  Defaults to `None` and not default will be provided.
        """
        # If the provided argument is required, but has a default, it isn't really required.
        # Raise an error so the user knows to correct the problem.
        # We need to utilize the default value to store the env value is it is required.
        if required and default:
            raise ValueError("You cannot provide a default value while requiring the value exists."
                             "Environment variable: " + env_name)

        # Fetch the env value, and return None if it doesn't exist.
        env_value = os.environ.get(env_name)

        # If the environment variable is not None.
        if env_value:
            # Since we have the env, set the default value to the environment variable.
            # Then, if it doesn't exist on the command-line, we can pull from here.
            default = env_value

            # In order for argparse to not error if we don't see the value on the command-line, we
            # need to set required to False.  Then, it will use the default we set.
            if required:
                required = False

        # Call the parent constructor to let argparse handle the rest of the arguments.
        super().__init__(default=default, required=required, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        # Set the specific attribute based on the name of the argument provided to argparse.
        setattr(namespace, self.dest, values)


def main() -> int:
    """Provides an argument parsing example for using the EnvironmentVariable class.

    Returns:
        int: The status code of the program.
    """

    parser = argparse.ArgumentParser("Example usage of the EnvironmentVariable class.")

    # You simply provide the class as an action, and set the env_name value.
    parser.add_argument("--url", "-u", required=True, action=EnvironmentVariable,
                        env_name=URL_ENV_NAME,
                        help="This argument can accept values via the environment variable " +
                        URL_ENV_NAME + ".")
    # You can provide defaults if the argument isn't required by command-line OR environment
    # variable, but you still want to check both places.
    parser.add_argument("--ignore", "-i", required=False, action=EnvironmentVariable,
                        env_name=IGNORE_ENV_NAME, default=False,
                        help="This argument can accept values via the environment variable " +
                        IGNORE_ENV_NAME + ".")

    # Once parsing happens, if it was required but not found as an env value, it will use the
    # command-line.  If it doesn't exist on the command-line, it will error as per usual.
    args = parser.parse_args()

    print(args)

    return SUCCESS


if __name__ == "__main__":
    sys.exit(main())
