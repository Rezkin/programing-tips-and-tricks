# Programming Tips and Tricks

[[_TOC_]]

A repository containing various cool tricks, code snippets, functions, and logic for various programming languages.  Consider this a "kitchen sink" grouping of interesting scripts and classes I've found/developed over the years, just so I don't forget.

Each section is thoroughly documented as a way to explain what is happening.  This README will act as a hub and summary of the content within.

## Python

### Template

A boiler-plate Python script to use when developing a new application.  For the most part, Python requires very little actual startup code, but this contains a bunch of nice-to-have features such as an initial `argparse` setup and debug handling.

See [template.py](python/template.py).

### Environment Variable Arguments

A class which handles reading in environment variables from the system as part of `argparse` instead of (or in addition to) command-line arguments.

See [env_args.py](python/env_args.py).

## Java

**To be filled out.**
